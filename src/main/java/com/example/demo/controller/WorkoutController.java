package com.example.demo.controller;

import com.example.demo.repository.WorkoutRepository;
import com.example.demo.domain.Workout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Yusuf on 23-Sep-17.
 */
@RestController
@RequestMapping("/jahmale")
public class WorkoutController {

    @Autowired
    private WorkoutRepository repository;

    @RequestMapping(path = "/save", method = RequestMethod.POST,consumes = "application/json")
    public Workout saveWorkout(Workout workout){
        return workout;
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    public Workout getWorkout(){
        Iterable<Workout> workouts = repository.findAll();
        return workouts.iterator().next();
    }
}
