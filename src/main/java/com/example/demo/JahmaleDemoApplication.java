package com.example.demo;

import com.example.demo.domain.Exercise;
import com.example.demo.domain.Workout;
import com.example.demo.domain.WorkoutSet;
import com.example.demo.repository.WorkoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication(exclude = RepositoryRestMvcAutoConfiguration.class)
public class JahmaleDemoApplication {

	@Autowired
	private WorkoutRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(JahmaleDemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		Exercise exercise = new Exercise();
		exercise.setDescription("Push metal bar");
		exercise.setTitle("Bench press");

		Exercise exercise1 = new Exercise();
		exercise.setDescription("Couch stuff");
		exercise.setTitle("Couch press");

		WorkoutSet workoutSet = new WorkoutSet();
		workoutSet.setReps(13);
		workoutSet.setExercise(exercise);
		workoutSet.setWeight(25d);

		WorkoutSet workoutSet1 = new WorkoutSet();
		workoutSet.setReps(13);
		workoutSet.setExercise(exercise1);
		workoutSet.setWeight(25d);


		Workout workout = new Workout();
		workout.setDescription("Chest Day");
		workout.setWorkoutSets(Arrays.asList(workoutSet, workoutSet1));



		return args -> {
			repository.save(workout);
		};
	}
}
