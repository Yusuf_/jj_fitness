package com.example.demo.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Yusuf on 23-Sep-17.
 */
@Data
@NoArgsConstructor
@Entity
public class Workout {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="WORKOUT_SET_ID")
    private List<WorkoutSet> workoutSets;
    private String description;
}
