package com.example.demo.domain;

import com.example.demo.domain.Exercise;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Yusuf on 23-Sep-17.
 */
public interface ExerciseWorkout extends CrudRepository<Exercise, Long> {
}
