package com.example.demo.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/**
 * Created by Yusuf on 23-Sep-17.
 */
@NoArgsConstructor
@Data
@Entity
public class WorkoutSet {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "EXERCISE_ID")
    private Exercise exercise;
    private Double weight;
    private Integer reps;
}
