package com.example.demo.repository;

import com.example.demo.domain.Workout;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Yusuf on 23-Sep-17.
 */
public interface WorkoutRepository extends CrudRepository<Workout, Long> {
}
